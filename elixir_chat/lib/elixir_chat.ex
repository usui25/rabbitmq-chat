defmodule ElixirChat do
  use AMQP

  # Initializes the application
  # Asks for the name of the user and joins super.chat room
  def start do
    user = IO.gets("Type in your name: ") |> String.strip
    IO.puts "Hi #{user}, you just joined a chat room! Type your message in and press enter."

    {:ok, conn} = AMQP.Connection.open
    {:ok, channel} = AMQP.Channel.open(conn)
    {:ok, queue_data } = AMQP.Queue.declare channel, ""

    AMQP.Exchange.fanout(channel, "super.chat")
    AMQP.Queue.bind channel, queue_data.queue, "super.chat"

    listen_for_messages(channel, queue_data.queue)
    wait_for_message(user, channel)
  end

  # Waits for the user input in a infinite loop
  def wait_for_message(user, channel) do
    message = IO.gets("") |> String.strip
    publish_message(user, message, channel)
    wait_for_message(user, channel)
  end

  # Waits for message on the subscribed channel
  # decodes it and outputs to the user
  def listen_for_messages(channel, queue_name) do
    AMQP.Queue.subscribe channel, queue_name, fn(payload, _meta) ->
      {:ok, data} = JSON.decode(payload)
      IO.puts "#{data["user"]}: #{data["message"]}"
    end
  end

  # Sends message to channel in the proper format
  def publish_message(user, message, channel) do
    { :ok, data } = JSON.encode([user: user, message: message])
    AMQP.Basic.publish channel, "super.chat", "", data
  end

end

ElixirChat.start
