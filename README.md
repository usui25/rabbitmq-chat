# Chat application

This is a chat application for communicating between two clients.It needs RabbitMq server for running properly.

## Starting ruby chat

You need to have ruby and proper gems installed on your machine. 

To run the ruby client:

* go to the ruby_chat directory
* use bunler to install your gems: bundle install
* in the console type:
``` 
ruby chat.rb 
```
## Starting elixir chat

You need to have erlang and elixir installed on your machine.

To run the elixir client:

* go to the elixir_chat directory
* to compile and run the project for the first time type:
``` 
mix compile
``` 
(this should install all the required dependencies)

* next time you want to run the project, just type: 
``` 
iex -S mix
``` 

## License

Released under the MIT license.